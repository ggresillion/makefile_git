CC = x86_64-w64-mingw32-gcc
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)

all : hello

hello: $(OBJ)
	@echo "phase de compilation"
	$(CC) -o hello hello.o message.o print.o
	@echo "executable $@ créé"

hello.o: message.h print.h

%.o: %.c
	$(CC) -o $@ -c $<

.PHONY: clean cleanall

clean:
	rm -f *.o

cleanall: clean
	rm -f *.exe